from selenium import webdriver

from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC

from Pom.dou_main_page import DouMainPage

driver = webdriver.Chrome()

driver.get("https://dou.ua/")
element = driver.find_element(By.XPATH, "//a[contains(@href, 'https://dou.ua/')]")


# wait = WebDriverWait(webdriver, 10)
# element = wait.until(EC.visibility_of(driver.find_element(By.XPATH, "//a[contains(@href, 'https://dou.ua/')]")))

assert element.is_displayed()

# def test_hw14_2(self, driver):
#     self.dou_logo = driver.find_element(By.XPATH, "//a[contains(@href, 'https://dou.ua/')]")
#     self.golovna_page_link = driver.find_element(By.XPATH, "//a[contains(text(),'Головна')]")
#     self.login_signin_link = driver.find_element(By.XPATH, "//a[@id='login-link']")
#     self.login_use_social_network_popup = driver.find_element(By.XPATH,
#                                                                    "(.//*[normalize-space(text()) and normalize-space(.)='Cкористайтесь акаунтом'])[1]/following::div[2]")
#

#
# assert test_hw14_2.dou_logo.is_displayed()
# assert test_hw14_2.golovna_page_link.is_displayed()
# selected_page_class = test_hw14_2.golovna_page_link.get_attribute("class")
# assert selected_page_class == "sel"
# assert test_hw14_2.login_signin_link.is_displayed()
# test_hw14_2.login_signin_link.click()
# assert test_hw14_2.login_use_social_network_popup.is_displayed()
