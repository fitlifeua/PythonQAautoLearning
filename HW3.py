import random
import re
letter_collection = ['o', 'O', 'о', 'О']

# Задача 1
# Напишіть цикл, який буде вимагати від користувача ввести слово, в якому є буква "о"
# (враховуються як великі так і маленькі). Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

print('Завдання 1')

while True:
    word = input("Будь-ласка введіть будь-яке слово, що містить літеру 'о': ")
    if not any(letter.isalpha() for letter in word):  # перевірка на валідність введених даних
        print(f"'{word}'- це зовсім не слово")
        quit()
    else:
        if any(letter in word for letter in letter_collection):
            print(f"У Вашому слові '{word}' є літера 'o'. Дякую")
            break
        else:
            print(f"Слово '{word}' не має літери 'о'. Будь-ласка спробуйте ще раз.")

# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску.

print('\nЗавдання 2')

lst1 = []

# Генеруємо рандомні дані для lst1
for elem in range(6):
    data_type = random.choice(['boolean', 'integer', 'string', 'none', 'float', 'list'])
    if data_type == 'boolean':
        lst1.append(random.choice([True, False]))
    elif data_type == 'integer':
        lst1.append(random.randint(0, 100))
    elif data_type == 'string':
        lst1.append(random.choice(['a', 'b', 'c', 'sTrInG', 'StRiNg', 'Ukraine']))
    elif data_type == 'none':
        lst1.append(None)
    elif data_type == 'float':
        lst1.append(random.uniform(0, 1))
    elif data_type == 'list':
        lst1.append([random.randint(0, 100) for i in range(5)])

print("Ваш сформований list: ", lst1)

lst2 = []
for my_str in lst1:
    if type(my_str) == str:
        lst2.append(my_str)
if len(lst2) == 0:
    print("Вибачте, цього разу стрінга у lst1 не було")
else:
    print("Ось Ваш lst2 із стрінгом із lst1: ", lst2)

# Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на "о"
# (враховуються як великі так і маленькі)

print('\nЗавдання 3. Варіант 1. Із регулярними виразами')

my_text = input("Введіть Ваш текст: ")

no_punctuation = re.sub('[^a-zA-Z]', ' ', my_text)  # Видаляємо усе окрім літер

words = no_punctuation.split()  # Ділимо стрінг на окремі слова

# Рахуємо слова із літерою "o" наприкінці
word_with_o = 0
for word_task3 in words:
    if word_task3.endswith('о' or 'О' or 'o' or 'O'):
        word_with_o += 1
print(f"Кількість слів із літерою 'О' наприкінці: {word_with_o}")

print('\nЗавдання 3. Варіант 2')

my_text2 = input("Введіть Ваш текст: ")

# Видаляємо усе окрім літер
no_punctuation2 = ""
for letter2 in my_text2:
    if letter2.isalpha():
        no_punctuation2 += letter2
    else:
        no_punctuation2 += " "

words2 = no_punctuation2.split()  # Ділимо стрінг на окремі слова

# Рахуємо слова із літерою "o" наприкінці
word_with_o_2 = 0
for word_task3_2 in words2:
    if word_task3_2.endswith('о') or word_task3_2.endswith('О')\
            or word_task3_2.endswith('o') or word_task3_2.endswith('O'):
        word_with_o_2 += 1
print(f"Кількість слів із літерою 'О' наприкінці: {word_with_o_2}")
