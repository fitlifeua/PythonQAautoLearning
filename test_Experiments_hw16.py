from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver

class Checkbox:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)
        self.url = "https://demoqa.com/checkbox"

    def navigate(self):
        self.driver.get(self.url)
        self.driver.maximize_window()

    def select_checkboxes(self, element_names):
        for element_name in element_names:
            expand_xpath = f"(.//*[normalize-space(text()) and normalize-space(.)='{element_name}'])[1]/preceding::*[name()='svg'][3]"
            checkbox_xpath = f"(.//*[normalize-space(text()) and normalize-space(.)='{element_name}'])[1]/preceding::*[name()='svg'][2]"
            expand = self.driver.find_element(By.XPATH, expand_xpath)
            checkbox = self.driver.find_element(By.XPATH, checkbox_xpath)
            expand.click()
            checkbox.click()
"""
    def get_checkboxes_status(self):
        checkboxes = self.wait.until(EC.presence_of_all_elements_located((By.XPATH, "(.//*[normalize-space(text()) and normalize-space(.)='Home'])[1]/preceding::*[name()='svg'][2]")))
        checkbox_status = {}
        for checkbox in checkboxes:
            checkbox_name = checkbox.text
            checkbox_checked = checkbox.find_element(By.XPATH, "../preceding-sibling::span").get_attribute("class") == "rct-icon rct-icon-check"
            checkbox_status[checkbox_name] = checkbox_checked
        return checkbox_status

"""
def test_checkboxes():
    driver = webdriver.Firefox()
    checkbox_page = Checkbox(driver)
    checkbox_page.navigate()

    elements_to_select = ["Home", "Desktop", "Commands", "Documents", "Office", "General"]
    checkbox_page.select_checkboxes(elements_to_select)

    checkboxes_status = checkbox_page.get_checkboxes_status()
    expected_checkboxes_status = {"Home": True,
                                  "Desktop": True,
                                  "Commands": True,
                                  "Documents": False,
                                  "Office": False,
                                  "General": True}

    assert checkboxes_status == expected_checkboxes_status, f"Expected {expected_checkboxes_status}," \
                                                            f"but got {checkboxes_status}"

    driver.quit()
