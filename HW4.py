# Задача 1
# Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить (не створить новий, а саме видалить!) з нього всі числа, які менше 21 і більше 74.

print('Завдання 1')

my_list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
print(f'Оригінальний List: {my_list}')
number_index = 0
while number_index < len(my_list):
    if my_list[number_index] < 21 or my_list[number_index] > 74:
        my_list.pop(number_index)
    else:
        number_index += 1
print(f'List після видалення всіх чисел, що менше 21 і більше 74: {my_list}')

#  Задача 2
# Є два довільних числа які відповідають за мінімальну і максимальну ціну.
# Є Dict з назвами магазинів і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245,
# "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "roze-tka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких потрапляють в діапазон
# між мінімальною і максимальною ціною.

# Наприклад:
# lower_limit = 35.9
# upper_limit = 37.339
# > match: "x-store", "main-service"

print('\nЗавдання 2')

lower_limit = 35.9
upper_limit = 37.339

print(f'lower_limit = {lower_limit} \nupper_limit = {upper_limit}')

stores_and_prices = {"cito": 47.999,
                     "BB_studio": 42.999,
                     "momo": 49.999,
                     "main-service": 37.245,
                     "buy.now": 38.324,
                     "x-store": 37.166,
                     "the_partner": 38.988,
                     "store": 37.720,
                     "roze-tka": 38.003}

#  Прохидимо по словнику та знаходимо значення, що входять у заданий діапазон між мінімальною і максимальною ціною
match = []
for store, price in stores_and_prices.items():
    if lower_limit <= price <= upper_limit:
        match.append(store)

#  Виводимо перелік магазинів (ключів), ціни яких потрапляють у заданий діапазон між мінімальною і максимальною ціною
print(f"Match: {', '.join(match)}")
