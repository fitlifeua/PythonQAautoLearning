# Задача 1: Створіть дві змінні first=10, second=30.
# Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел
print('Завдання 1')

first = 10
second = 30

print('\nВаріант 1 (вивід через String)')
summ = first+second
difference = second-first
multiply = first*second
division = second/first

print('first + second = ' + str(summ)
      + '\nsecond - first = ' + str(difference)
      + '\nfirst x second = ' + str(multiply)
      + '\nsecond / first = ' + str(division))

print('\nВаріант 2')
print('first + second = ', summ)
print('second - first = ', difference)
print('first x second = ', multiply)
print('second / first = ', division)

print('\nВаріант 3')
print("sum: ", first + second)
print("difference: ", second - first)
print("multiply: ", first * second)
print("division: ", first / second)

# Задача 2: Створіть змінну и по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
# Виведіть на екран результат кожного порівняння.

print('\nЗавдання 2')

compare_result = first < second
print('first < second = ' + str(compare_result))
compare_result = first > second
print('first > second = ' + str(compare_result))
compare_result = first == second
print('first == second = ' + str(compare_result))
compare_result = first != second
print('first != second = ' + str(compare_result))

# Задача 3: Створіть змінну - результат конкатенації строк "Hello " та "world!".

print('\nЗавдання 3')
hello_world = 'Hello' + ' world!'
print(hello_world)

#push to master, not main