import datetime

import time


def time_checker(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Функція {func.__name__} виконується за {end_time - start_time} секунд")
        return result
    return wrapper


def season_checker(date):
    """
    Функція перевіряє сезон за введеною датою. Дата вводиться у форматі "[дд].[мм]"
    Args:
        season_date: приймає str

    Returns: Після перевірки дати, повертає стрінг з відповідним сезоном.
    """
    day, month = date.split(".")
    month = int(month)
    if month in [12, 1, 2]:
        return "Зима"
    elif month in [3, 4, 5]:
        return "Весна"
    elif month in [6, 7, 8]:
        return "Літо"
    elif month in [9, 10, 11]:
        return "Осінь"


def date_validation(date):
    """
    Функція перевіряє валідність введеної дати. Використовується бібліотека datetime.
    Для перевірки валідності введеної дати приймає стрінг з відповідним сезоном, до якого відноситься.
    Слід зауважити, що для більш детальної перевірки необхідно для параметру year зазначити рік для якого буде
    записана дата.
    Args:
        valid_date: приймає str

    Returns:
        Повертає помилку, якщо введена невірна дата.
    """
    day, month = date.split('.')
    day = int(day)
    month = int(month)
    try:
        datetime.datetime(year=2023, month=month, day=day)
        return True
    except ValueError:
        return False


# date = input("Будь-ласка введіть дату у форматі '[дд].[мм]': ")
# date = str
# if date_validation(date):
#     print(season_checker(date))
# else:
#     print("Такої дати не існує")


def stupid_calculator(num1: int or float, num2: int or float, operation: str):
    """
    Функція приймає два числових аргументи і строковий, який відповідає за операцію між ними (+ - / *)
    Args:
        num1: перший числовий аргумент. Приймається тільки типи int або float.
        num2: другий числовий аргумент. Приймається тільки типи int або float.
        operation: строковий аргумент який буде відповідати за операцію між першим та другим аргументами.
                   Приймається данні типу str та тільки які відповідають базовим арефметичним оперціям.
                   А саме "+" для додавання, "-" для віднімання, "/" для ділення, "*" для множення.

    Returns: повертає значення операції за заданими аргументами. У разі введення у num1 та/або num2 неприпустимого
             типу повертається None і виводиться повідомлення "Невірний тип даних"
             У разі введення у operation неприпустимого типу або неприпустиму арефмитичну дію повертається None
             і виводиться повідомлення "Операція не підтримується"
    """
    if (type(num1) != int and type(num1) != float) or (type(num2) != int and type(num2) != float):
        print("Невірний тип даних")
        return None
    elif operation == "+":
        return num1 + num2
    elif operation == "-":
        return num1 - num2
    elif operation == "/":
        return num1 / num2
    elif operation == "*":
        return num1 * num2
    else:
        print("Операція не підтримується")
        return None
