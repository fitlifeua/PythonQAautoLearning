from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver


class RadioButton:
    def __init__(self, driver):
        self.driver = driver
        self.radio_buttons = {
            "Yes": (By.XPATH, "//label[@for='yesRadio']"),
            "Impressive": (By.XPATH, "//label[@for='impressiveRadio']"),
            "No": (By.XPATH, "//label[@for='noRadio']"),
            "No, its not required": (By.XPATH, "//label[@for='noRadio']//following-sibling::label")
        }

    def activate_radio_button(self, button_name):
        button = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.radio_buttons[button_name]))
        button.click()

    def is_radio_button_selected(self, button_name):
        button = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.radio_buttons[button_name]))
        return button.find_element(By.XPATH, "./preceding-sibling::input").is_selected()

    def get_radio_buttons_info(self):
        radio_buttons_data = {}
        for button_name in self.radio_buttons:
            button = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.radio_buttons[button_name]))
            input_element = button.find_element(By.XPATH, "./preceding-sibling::input")
            enabled = input_element.is_enabled()
            selected = input_element.is_selected()
            radio_buttons_data[button_name] = {"Enabled": enabled, "Selected": selected}
        return radio_buttons_data

def test_radiobutton():
    driver = webdriver.Chrome()
    driver.get("https://demoqa.com/radio-button")
    driver.maximize_window()
    radio_button = RadioButton(driver)

    # (test_activate_yes_radio) Активувати кнопку Yes та переконатись що вона активована.
    # Переконатись двома різними способами.
    radio_button.activate_radio_button("Yes")
    assert radio_button.is_radio_button_selected("Yes")
    assert driver.find_element(By.ID, "yesRadio").is_selected()

    # (Бонус)(test_activate_disabled_radio_button) Увімкнути та активувати кнопку No,
    # переконатись в тому, що вона обрана
    driver.execute_script("arguments[0].removeAttribute('disabled')", driver.find_element(By.XPATH, "//label[@for='noRadio']"))
    radio_button.activate_radio_button("No")
    assert radio_button.is_radio_button_selected("No")
    assert driver.find_element(By.ID, "noRadio").is_selected()

    # (test_get_radio_buttons_info) Визначити які є радіо-баттони на сторінці та сформувати словник із такими даними:
    # Назва кнопки, Чи увімкнена кнопка, Чи активна (обрана) кнопка
    radio_buttons_info = radio_button.get_radio_buttons_info()
    assert radio_buttons_info == {
        "Yes": {"Enabled": True, "Selected": True},
        "Impressive": {"Enabled": True, "Selected": False},
        "No": {"Enabled": True, "Selected": False},
        "No, its not required": {"Enabled": False, "Selected": False}
    }

    driver.quit()
