import os

from datetime import datetime

from HW13 import get_exchange_rates, write_exchange_rates_to_file


def test_get_exchange_rates():
    exchange_rates = get_exchange_rates()
    assert len(exchange_rates) > 0
    for rate in exchange_rates:
        assert " to UAH: " in rate


def test_write_exchange_rates_to_file():
    file_path = "test_file.txt"
    exchange_rates = ["USD to UAH: 27.00", "EUR to UAH: 32.00"]
    write_exchange_rates_to_file(exchange_rates, file_path)
    assert os.path.exists(file_path)

    with open(file_path, "r") as file:
        lines = file.readlines()
        assert len(lines) == len(exchange_rates) + 1  # +1 for the date line

        assert datetime.strptime(lines[0].strip(), "[%Y-%m-%d %H:%M:%S]") is not None

        for i in range(1, len(lines)):
            line = lines[i].strip()
            parts = line.split(": ")
            assert len(parts) == 2  # line should contain ": "
            assert parts[0].startswith(str(i) + ". ")  # line format
            assert float(parts[1]) > 0  # exchange rate should be positive

    os.remove(file_path)
