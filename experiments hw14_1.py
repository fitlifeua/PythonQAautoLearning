from Pom.dou_main_page import DouMainPage


def test_hw14(wo_login_user) -> None:
    driver = wo_login_user
    main_page = DouMainPage(driver)

    assert main_page.dou_logo.is_displayed()
    assert main_page.golovna_page_link.is_displayed()
    selected_page_class_attribute = main_page.golovna_page_link.get_attribute("class")
    assert selected_page_class_attribute == "sel"
    assert main_page.login_signin_link.is_displayed()
    main_page.login_signin_link.click()
    assert main_page.login_use_social_network_popup.is_displayed()
