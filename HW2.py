# Задача 1
# Сформуйте стрінг, в якому міститься інформація про певне слово.
# "Word [тут слово] has [тут довжина слова, отримайте з самого сдлва] letters", наприклад "Word 'Python' has 6 letters".
# Для отримання слова для аналізу скористайтеся константою або функцією input().

print('Завдання 1')

my_word = input('Insert the word here: ')

print(f'The word "{my_word}" has {len(my_word)} letters')

# Напишіть программу "Касир в кінотеватрі", яка попросіть користувача ввести свсвій вік
# (можно використати константу або функцію input(), на екран має бути виведено лише одне повідомлення,
# також подумайте над варіантами, коли введені невірні дані).
# a) якщо користувачу менше 7 - вивести повідомлення "Де твої батьки?"
# b) якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
# c) якщо користувачу більше 65 - вивести повідомлення"Покажіть пенсійне посвідчення!"
# d) якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
# e) у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"

print('\nЗавдання 2')
print('\nВаріант 1. Виконано по DRY')

client_age = input('insert the age of the client: ')
if not client_age.isdigit() or int(client_age) <= 0:
    print('error! the age must be only positive numbers')  # перевірка на валідність введених даних

else:
    age = int(client_age)
    if str(7) in str(age):
        print("вам сьогодні пощастить!")
    elif age < 7:
        print('де твої батьки?')
    elif age < 16:
        print('цей фільм для дорослих!')
    elif 65 <= age <= 100:
        print('покажіть пенсійне посвідчення!')
    else:
        print('а білетів вже немає!')

print('\nВаріант 2. Не дуже по DRY')

client_age_v2 = input('Insert the age of the client: ')

if not client_age_v2.isdigit() or int(client_age_v2) <= 0:
    print('Error! The age must be only positive numbers')  # перевірка на валідність введених даних
elif str(7) in str(client_age_v2):
    print("Вам сьогодні пощастить!")
elif int(client_age_v2) < 7:
    print('Де твої батьки?')
elif int(client_age_v2) < 16:
    print('Цей фільм для дорослих!')
elif 65 <= int(client_age_v2) <= 100:
    print('Покажіть пенсійне посвідчення!')
else:
    print('А білетів вже немає!')
