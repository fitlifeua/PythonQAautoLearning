# 1. Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані
# від "Транспортний засіб". Наповніть класи атрибутами на свій розсуд. Створіть обʼєкти класів "Автомобіль",
# "Літак", "Корабель"


class Vehicle:
    def __init__(self, trade_mark, model, year, color, weight, price):
        self.trade_mark = trade_mark
        self.model = model
        self.year = year
        self.color = color
        self.weight = weight
        self.price = price


class Car(Vehicle):
    def __init__(self, trade_mark, model, year, color, weight, price, transmition, num_wheels, num_doors):
        super().__init__(trade_mark, model, year, color, weight, price)
        self.trasmition = transmition
        self.num_wheels = num_wheels
        self.num_doors = num_doors


class Airplane(Vehicle):
    def __init__(self, trade_mark, model, year, color, weight, price, num_engines, num_passangers):
        super().__init__(trade_mark, model, year, color, weight, price)
        self.num_engines = num_engines
        self.num_passangers = num_passangers


class Ship(Vehicle):
    def __init__(self, trade_mark, model, year, color, weight, price, water_displacement, floatage):
        super().__init__(trade_mark, model, year, color, weight, price)
        self.water_displacement = water_displacement
        self.floatage = floatage


# Створюємо об'єкти класів
my_car = Car("Toyota", "Rav4", 2022, "white", 1550, 30000, "Automatic", 4, 4)
my_airplane = Airplane("Boeing", "777X", 2025, "white-blue", 181436.948, 3000000, 8, 500)
my_ship = Ship("Flying Hollander", "Smile", 1666, "black", 7000, 6666, 7000, 10)

# example
print(f'Your vehicle is a {Ship.__name__}, by the legendary "{my_ship.trade_mark}" trade mark.\n'
      f'the model is "{my_ship.model}", the year of production is {my_ship.year}. The color is {my_ship.color}\n'
      f'the weight is {my_ship.weight}, the price is {my_ship.price}\n'
      f'and the water displacement is {my_ship.water_displacement}\n')
