import datetime


def season_checker(season_date):
    day, month = season_date.split(".")
    month = int(month)
    if month in [12, 1, 2]:
        return "Зима"
    elif month in [3, 4, 5]:
        return "Весна"
    elif month in [6, 7, 8]:
        return "Літо"
    elif month in [9, 10, 11]:
        return "Осінь"


def date_validation(valid_date):
    day, month = valid_date.split('.')
    day = int(day)
    month = int(month)
    try:
        datetime.datetime(year=2023, month=month, day=day)
        return True
    except ValueError:
        return False


def stupid_calculator(num1: int or float, num2: int or float, operation: str):
    if (type(num1) != int and type(num1) != float) or (type(num2) != int and type(num2) != float):
        print("Невірний тип даних")
        return None
    elif operation == "+":
        return num1 + num2
    elif operation == "-":
        return num1 - num2
    elif operation == "/":
        return num1 / num2
    elif operation == "*":
        return num1 * num2
    else:
        print("Операція не підтримується")
        return None
