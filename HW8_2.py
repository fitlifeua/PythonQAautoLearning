from lib2 import date_validation, season_checker, stupid_calculator

import time


# 1. Напишіть декоратор, який визначає час виконання функції.
# Заміряйте час іиконання функцій з попереднього ДЗ


def time_checker(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Функція {func.__name__} виконується за {end_time - start_time} секунд")
        return result
    return wrapper


@time_checker
def hw8_task1(num1, num2, operator):
    return stupid_calculator(num1, num2, operator)


num11 = 4
num12 = 2.5
operator = "*"

result = hw8_task1(num11, num12, operator)

if result is not None:
    print("Відповідь: ", result)


# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання

date = input("Будь-ласка введіть дату у форматі '[дд].[мм]': ")
if date_validation(date):
    print(season_checker(date))
else:
    print("Такої дати не існує")
