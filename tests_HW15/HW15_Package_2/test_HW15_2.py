"""
3 тести, знаходяться поза класом (в тестовому файлі взагалі відсутній клас).
На кожному тесті -- мітка 'param'. Тести мають бути параметризовані: перший протестує одиночні
параметри, другий -- подвійні, третій -- одиночні, але з присвоєнням псевдонімів.
На кожен тест має діяти фікстура, що знаходиться на рівні проекту та виводить повідомлення перед
та після кожного тесту. В сигнатурі тесту має бути пусто (не викликати фікстуру примусово).
"""

import pytest


@pytest.mark.param
@pytest.mark.parametrize("input", [1, 2, 3])
def test_single_param(input):
    assert input > 0


@pytest.mark.param
@pytest.mark.parametrize("input1, input2", [(1, 2), (3, 4), (5, 6)])
def test_double_param(input1, input2):
    assert input1 < input2


@pytest.mark.param
@pytest.mark.parametrize("input", [(1, "one"), (2, "two"), (3, "three")], ids=["first", "second", "third"])
def test_param_with_aliases(input):
    assert isinstance(input[0], int)
    assert isinstance(input[1], str)
