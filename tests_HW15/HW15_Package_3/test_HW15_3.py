"""
3 (або більше) тести, організовані довільно (в класі чи поза ним).
На кожному тесті 2 мітки: одна спільна для усіх тестів в цьому пакунку,
наприклад 'pack', друга -- спільна для частини тестів, наприклад для двох;
остатня 'rest' -- для тих тестів що залишились.
"""

import pytest


@pytest.fixture(scope="session", autouse=True)
def run_around_tests():
    print("Starting SESSION tests...")
    yield
    print("All tests completed.")


@pytest.mark.pack
@pytest.mark.two
def test_one():
    pass


@pytest.mark.pack
@pytest.mark.two
@pytest.mark.rest
def test_two():
    pass


@pytest.mark.pack
def test_three():
    pass


@pytest.mark.pack
@pytest.mark.rest
def test_four():
    pass
