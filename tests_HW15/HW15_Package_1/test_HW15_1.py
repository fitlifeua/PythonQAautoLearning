"""
5 тестів, знаходяться в класі. Тести можуть бути пусті (pass).
На кожному тесті мітка (mark) 'from_class'. Перед тестами та після них має відпрацювати фікстура,
яка виведе повідомлення про початок тестів, та про їх завершення. Має бути одне повідомлення про початок, та одне
повідомлення про завершення тестів.
"""

import pytest


class TestClass:
    @pytest.fixture(scope="class", autouse=True)
    def run_class_tests(self):
        print("Starting CLASS tests...")
        yield
        print("All tests completed.")

    @pytest.mark.from_class
    def test_example1(self):
        assert True

    @pytest.mark.from_class
    def test_example2(self):
        assert True

    @pytest.mark.from_class
    def test_example3(self):
        assert True

    @pytest.mark.from_class
    def test_example4(self):
        assert True

    @pytest.mark.from_class
    def test_example5(self):
        assert True
