import time

# 1. Напишіть декоратор, який визначає час виконання функції. Заміряйте час іиконання функцій з попереднього ДЗ


def time_checker(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(f"Функція {func.__name__} виконується за {end_time - start_time} секунд")
        return result
    return wrapper


@time_checker
def stupid_calculator(num1: int or float, num2: int or float, operation: str):
    if (type(num1) != int and type(num1) != float) or (type(num2) != int and type(num2) != float):
        print("Невірний тип даних")
        return None
    elif operation == "+":
        return num1 + num2
    elif operation == "-":
        return num1 - num2
    elif operation == "/":
        return num1 / num2
    elif operation == "*":
        return num1 * num2
    else:
        print("Операція не підтримується")
        return None


num1 = 2
num2 = 2.5
operator = "*"

result = stupid_calculator(num1, num2, operator)

if result is not None:
    print("Відповідь: ", result)


@time_checker
def check_num1_type(num1_2):
    if type(num1_2) != int and type(num1_2) != float:
        print("Невірний тип даних")
        return False
    else:
        return True


@time_checker
def check_num2_type(num2_2):
    if (type(num2_2) != int and type(num2_2) != float):
        print("Невірний тип даних")
        return False
    else:
        return True


@time_checker
def check_operator_type(operation_2):
    if operation_2 == "+" or operation_2 == "-" or operation_2 == "/" or operation_2 == "*":
        return True
    else:
        print("Операція 2 не підтримується")
        return False


@time_checker
def check_result(num1_2, num2_2, operation_2):
    result_1_2 = check_num1_type(num1_2)
    result_2_2 = check_num2_type(num2_2)
    result_3_2 = check_operator_type(operation_2)
    if result_1_2 and result_2_2 and result_3_2:
        # return result_3_2
        return result_1_2 and result_3_2 and result_2_2
    else:
        return None


num1_2 = 3
num2_2 = 3
operation_2 = "-"

result_2 = check_result(num1_2, num2_2, operation_2)

if result_2 is not None:
    print("Відповідь 2: ", result_2)

# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання

