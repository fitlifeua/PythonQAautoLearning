import pytest


@pytest.fixture(autouse=True)
def run_project_tests():
    print("Starting tests...")
    yield
    print("All tests completed.")


"""
data from homework 14

from selenium import webdriver
import pytest
from Pom.dou_main_page import DouMainPage


@pytest.fixture(name="wo_login_user")
def wo_login_user_fixture():
    driver = webdriver.Chrome()
    driver.get("https://dou.ua/")

    yield DouMainPage(driver)
    driver.quit()
"""

"""
data from previous homeworks

from HW12 import Phone, Laptop, SmartWatch

@pytest.fixture(scope="class")
def phone1():
    return Phone('Apple', 'iPhone 14 Pro', 2022, 'iOS 15', 999, '128GB', '12MP')


@pytest.fixture(scope="class")
def phone2():
    return Phone('Samsung', 'Galaxy S22 Ultra', 2022, 'Android 12', 1199, '128GB', '108MP')


@pytest.fixture(scope="class")
def laptop1():
    return Laptop('Apple', 'MacBook Pro', 2022, 'macOS Monterey', 1299, '8GB', '13-inch')


@pytest.fixture(scope="class")
def laptop2():
    return Laptop('ASUS', 'ProArt Studiobook 16', 2021, 'Windows', 1999, '64GB', '16-inch')


@pytest.fixture(scope="class")
def smartwatch1():
    return SmartWatch('Apple', 'Watch Series 8', 2022, 399, 'watchOS 09', '18 hours', '45mm')


@pytest.fixture(scope="class")
def smartwatch2():
    return SmartWatch('Samsung', 'Galaxy Watch 5 Pro', 2022, 449, 'Tizen OS 6.0', '40 hours', '45mm')
"""
