# 1. Доопрацюйте класс Pоint з заняття наступним чином: додайте перевірку координат x та y на числа за допомогою property

print('Завдання 1')


class Point:
    def __init__(self, x_coord, y_coord):
        self._x = None
        self._y = None
        self.x = x_coord
        self.y = y_coord

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError("x має бути int або float")
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if not isinstance(value, (int, float)):
            raise TypeError("y має бути int або float")
        self._y = value


# 2. Доопрацюйте класс Line з заняття наступним чином: додайте можливість порівнювати лінії
# по довжині (==, !=, >=, <=, >, <) за допомогою відповідних методів

print('\nЗавдання 2')


class Line:
    begin = None
    end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    def _check_data_type(self, other):
        if not isinstance(other, Line):
            raise TypeError("Неможливо порівняти. Параметри мають різний тип даних")

    def length(self):
        return ((self.end.x - self.begin.x)**2 + (self.end.y - self.begin.y)**2)**0.5

    def __eq__(self, other):
        return self.length() == other.length()

    def __ne__(self, other):
        return self.length() != other.length()

    def __ge__(self, other):
        return self.length() >= other.length()

    def __le__(self, other):
        return self.length() <= other.length()

    def __gt__(self, other):
        return self.length() > other.length()

    def __lt__(self, other):
        return self.length() < other.length()


# Example

def compare_lines(line1, line2):
    if line1 == line2:
        print('Лінії рівні')
    else:
        print('Лінії не рівні')

    if line1 != line2:
        print('Лінії не рівні')
    else:
        print('Лінії рівні')

    if line1 >= line2:
        print('Лінія 1 більше або дорівнює Лінії 2')
    else:
        print('Лінія 1 не більше або дорівнює Лінії 2')

    if line1 <= line2:
        print('Лінія 2 більше або дорівнює Лінії 1')
    else:
        print('Лінія 2 не більше або дорівнює Лінії 1')

    if line1 > line2:
        print('Лінія 1 більше Лінії 2')
    else:
        print('Лінія 1 не більше Лінії 2')

    if line1 < line2:
        print('Лінія 2 більше Лінії 1')
    else:
        print('Лінія 2 не більше Лінії 1')

line1 = Line(Point(0, 0), Point(1, 6))
line2 = Line(Point(0, 0), Point(4, -2))

compare_lines(line1, line2)