#---conftest---
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service


@pytest.fixture(scope="class")
def chrome(request):
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-automation'])
    options.add_experimental_option('useAutomationExtension', False)
    options.add_argument('--disable-cache')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-infobars')
    options.add_argument('--disable-browser-side-navigation')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    service = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=service, options=options)
    driver.set_script_timeout(30)
    if request.cls:
        request.cls.driver = driver
    yield driver
    driver.quit()

#---checkboxes_element.py---
from selenium.webdriver.remote.webdriver import WebDriver

from Homework16CheckBoxes.widgets.base_widget import Component


class CheckBox(Component):
    def __init__(self, driver: WebDriver, locator: tuple, name: str = None):
        super().__init__(driver, locator)
        self.locator = locator
        by, loc = locator
        self.label = self.driver.find_element(by, loc)
        self.input_loc = '//input[@type="checkbox"]'
        self.input = self.label.find_element(By.XPATH, self.input_loc)
        self.named_label = '[contains(@for, "{}")]'
        self.named_input = '[contains(@id, "{}")]'
        if name:
            loc += self.named_label.format(name)
            self.label = self.driver.find_element(by, loc)
            self.input = self.label.find_element(By.XPATH, f'{self.input_loc}{self.named_input.format(name)}')

    def type_of(self) -> str:
        return self.__class__.__name__

    def __change_checkbox_selection_state(self, name: str) -> None:
        by, loc = self.locator
        if name:
            loc += self.named_label.format(name)
        label = self.driver.find_element(by, loc)
        label.click()

    def is_selected(self, name: str = None) -> bool:
        if name:
            input_loc = f'{self.input_loc}{self.named_input.format(name)}'
            input_ = self.label.find_element(By.XPATH, input_loc)
            return input_.is_selected()
        return self.input.is_selected()

    def select(self, name: str) -> None:
        if not self.is_selected(name):
            self.__change_checkbox_selection_state(name)

    def deselect(self, name: str) -> None:
        if self.is_selected(name):
            self.__change_checkbox_selection_state(name)

# expandable_elements.py
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.common import NoSuchElementException


class ExpandableTreeElement:
    def __init__(self, driver: WebDriver, locator: tuple):
        self.driver: WebDriver = driver
        self.locator = locator
        self.by, self.loc = locator
        self.expand_loc_name = self.loc + "[contains(@for, '{}')]//ancestor::span/button"
        self.folder_selection_state_loc_name = self.loc + "[contains(@for, '{}')]"
        self.folder_selection_state_loc_name_input = self.loc + "[contains(@for, '{}')]/input"

    def __change_folder_expand_state(self, name: str, expand_action: bool):
        loc_name = self.expand_loc_name.format(name)
        element = self.driver.find_element(self.by, loc_name)
        try:
            if expand_action:
                expand = element.find_element(By.XPATH, '//*[contains(@class, "expand-close")]')
                if expand.is_displayed():
                    element.click()
            else:
                collapse = element.find_element(By.XPATH, '//*[contains(@class, "expand-open")]')
                if collapse.is_displayed():
                    element.click()
        except NoSuchElementException:
            pass

    def expand_folder(self, name) -> None:
        self.__change_folder_expand_state(name=name, expand_action=True)

    def collapse_folder(self, name) -> None:
        self.__change_folder_expand_state(name=name, expand_action=False)

    def __change_folder_selection_state(self, name, enabled=False):
        loc_name = self.folder_selection_state_loc_name.format(name)
        loc_name_input = self.folder_selection_state_loc_name_input.format(name)
        element = self.driver.find_element(self.by, loc_name)
        element_input = self.driver.find_element(self.by, loc_name_input)
        if enabled:
            if not element_input.is_selected():
                element.click()
        else:
            if element_input.is_selected():
                element.click()

    def mark_folder(self, name):
        self.__change_folder_selection_state(name, enabled=True)

    def unmark_folder(self, name):
        self.__change_folder_selection_state(name, enabled=False)

#---radio_button.py---
from selenium.webdriver.remote.webdriver import WebDriver


class RadioButton:
    def __init__(self, driver: WebDriver, locator: tuple):
        self.driver: WebDriver = driver
        self.locator = locator
        self.by, self.loc = locator
        self.locator_name = self.loc + "[contains(@for, '{}')]"
        self.locator_input = "//ancestor::div/input"

    def type_of(self) -> str:
        return self.__class__.__name__

    def select(self, name):
        element = self.driver.find_element(self.by, self.locator_name.format(name))
        element.click()

    def is_selected(self, name: str = None) -> bool:
        element_check_locator = self.locator_name.format(name) + self.locator_input
        element = self.driver.find_element(self.by, element_check_locator)
        return element.is_selected()

    def check_radio_button_text_success_status(self, name):
        element = self.driver.find_element(By.XPATH,
                                           f"//span[@class ='text-success'][text()[contains(., '{name.title()}')]]")
        return element.is_displayed()

    def enable_radio_button(self, name):
        enable_element_locator = self.locator_name.format(name) + self.locator_input
        element = self.driver.find_element(self.by, enable_element_locator)
        if not element.is_enabled():
            self.driver.execute_script("arguments[0].disabled = false;", element)
        return self

    def collect_buttons(self):
        loc = '//input[@type="radio"]'
        collected_buttons = self.driver.find_elements(self.by, loc)
        collected_buttons_info = {}
        for elem in collected_buttons:
            name: str = elem.get_attribute("id")[
                        :-5]
            is_selected = elem.is_selected()
            is_enabled = elem.is_enabled()
            collected_buttons_info.update(
                {name: {'is activated': is_enabled, 'is selected': is_selected}})

        return collected_buttons_info

#---page_checkbox.py---
from selenium.webdriver.remote.webdriver import WebDriver
#from Homework16CheckBoxes.widgets.expandable_elements import ExpandableTreeElement


class PageCheckbox:
    _instance = None
    URL = "https://demoqa.com/checkbox"

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, driver: WebDriver):
        self.driver: WebDriver = driver
        self.folder_widget = ExpandableTreeElement(self.driver, (By.XPATH, '//label[contains(@for, "tree-node-")]'))

    def open(self):
        self.driver.get(self.URL)
        return self

    def page_expand_folder(self, name):
        self.folder_widget.expand_folder(name)

    def page_collapse_folder(self, name):
        self.folder_widget.collapse_folder(name)

    def page_mark_folder(self, name):
        self.folder_widget.mark_folder(name)

    def page_unmark_folder(self, name):
        self.folder_widget.mark_folder(name)

#---page_radio_button.py---
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
#from Homework16CheckBoxes.widgets.radio_button import RadioButton


class PageRadioButton:
    _instance = None
    URL = "https://demoqa.com/radio-button"

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, driver: WebDriver):
        self.driver: WebDriver = driver
        self.page_radio_button = RadioButton(self.driver, (By.XPATH, '//label[contains(@for, "Radio")]'))

    def open(self):
        self.driver.get(self.URL)
        return self

    def select_radio_button(self, name):
        self.page_radio_button.select(name)

    def is_selected_radio_button(self, name):
        return self.page_radio_button.is_selected(name)

    def check_radio_button_text_status(self, name):
        return self.page_radio_button.check_radio_button_text_success_status(name)

    def enable_radio_button(self, name):
        return self.page_radio_button.enable_radio_button(name)

    def collect_buttons_info(self):
        return self.page_radio_button.collect_buttons()

#---test_checkboxes.py---
import pytest

# from Homework16CheckBoxes.pages.page_checkbox import PageCheckbox

expand_elements_list = ['home', 'desktop', 'documents', 'office']
mark_elements_list = ['commands', 'general']


@pytest.mark.usefixtures('chrome')
class TestCheckboxes:

    def setup_method(self):
        self.driver: WebDriver = self.driver
        self.checkbox_page = PageCheckbox(self.driver).open()

    def test_checkboxes(self):
        for elem in expand_elements_list:
            self.checkbox_page.page_expand_folder(elem)
        for elem in mark_elements_list:
            self.checkbox_page.page_mark_folder(elem)
        pass

#---test_radio_button.py---
import pytest
from selenium.webdriver.remote.webdriver import WebDriver
import time

# from Homework16CheckBoxes.pages.page_radio_button import PageRadioButton


@pytest.mark.usefixtures('chrome')
class TestCheckboxes:

    def setup_method(self):
        self.driver: WebDriver = self.driver
        self.page_radio_button = PageRadioButton(self.driver).open()

    def test_activate_yes_radio(self):
        self.page_radio_button.select_radio_button('yes')
        assert self.page_radio_button.is_selected_radio_button('yes')

    def test_activate_yes_button_var2(self):
        self.page_radio_button.select_radio_button('yes')
        assert self.page_radio_button.check_radio_button_text_status('yes')

    def test_get_radio_buttons_info(self):
        self.page_radio_button.select_radio_button('yes')
        print(len(self.page_radio_button.collect_buttons_info()) == 3)

    def test_activate_disabled_button(self):
        self.page_radio_button.enable_radio_button('no')
        self.page_radio_button.select_radio_button('no')
        assert self.page_radio_button.is_selected_radio_button('no')

"""#---conftest.py---
import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(scope="class")
def chrome(request):
    options = webdriver.ChromeOptions()
    options.add_experimental_option('excludeSwitches', ['enable-automation'])
    options.add_experimental_option('useAutomationExtension', False)
    options.add_argument('--disable-cache')
    options.add_argument('--disable-extensions')
    options.add_argument('--disable-infobars')
    options.add_argument('--disable-browser-side-navigation')
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    service = Service(ChromeDriverManager().install())
    driver = webdriver.Chrome(service=service, options=options)
    driver.set_script_timeout(30)
    if request.cls:
        request.cls.driver = driver
    yield driver
    driver.quit()

#---checkboxes_element.py---
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from Homework16CheckBoxes.widgets.base_widget import Component


class CheckBox(Component):
    def __init__(self, driver: WebDriver, locator: tuple, name: str = None):
        super().__init__(driver, locator)
        self.locator = locator
        by, loc = locator
        self.label = self.driver.find_element(by, loc)
        self.input_loc = '//input[@type="checkbox"]'
        self.input = self.label.find_element(By.XPATH, self.input_loc)
        self.named_label = '[contains(@for, "{}")]'
        self.named_input = '[contains(@id, "{}")]'
        if name:
            loc += self.named_label.format(name)
            self.label = self.driver.find_element(by, loc)
            self.input = self.label.find_element(By.XPATH, f'{self.input_loc}{self.named_input.format(name)}')

    def type_of(self) -> str:
        return self.__class__.__name__

    def __change_checkbox_selection_state(self, name: str) -> None:
        by, loc = self.locator
        if name:
            loc += self.named_label.format(name)
        label = self.driver.find_element(by, loc)
        label.click()

    def is_selected(self, name: str = None) -> bool:
        if name:
            input_loc = f'{self.input_loc}{self.named_input.format(name)}'
            input_ = self.label.find_element(By.XPATH, input_loc)
            return input_.is_selected()
        return self.input.is_selected()

    def select(self, name: str) -> None:
        if not self.is_selected(name):
            self.__change_checkbox_selection_state(name)

    def deselect(self, name: str) -> None:
        if self.is_selected(name):
            self.__change_checkbox_selection_state(name)

#---expandable_elements.py---
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException


class ExpandableTreeElement:
    def __init__(self, driver: WebDriver, locator: tuple):
        self.driver: WebDriver = driver
        self.locator = locator
        self.by, self.loc = locator
        self.expand_loc_name = self.loc + "[contains(@for, '{}')]//ancestor::span/button"
        self.folder_selection_state_loc_name = self.loc + "[contains(@for, '{}')]"
        self.folder_selection_state_loc_name_input = self.loc + "[contains(@for, '{}')]/input"

    def __change_folder_expand_state(self, name: str, expand_action: bool):
        loc_name =         self.expand_loc_name.format(name)
        element = self.driver.find_element(self.by, loc_name)
        try:
            if expand_action:
                expand = element.find_element(By.XPATH, '//*[contains(@class, "expand-close")]')
                if expand.is_displayed():
                    element.click()
            else:
                collapse = element.find_element(By.XPATH, '//*[contains(@class, "expand-open")]')
                if collapse.is_displayed():
                    element.click()
        except NoSuchElementException:
            pass

    def expand_folder(self, name) -> None:
        self.__change_folder_expand_state(name=name, expand_action=True)

    def collapse_folder(self, name) -> None:
        self.__change_folder_expand_state(name=name, expand_action=False)

    def __change_folder_selection_state(self, name, enabled=False):
        loc_name = self.folder_selection_state_loc_name.format(name)
        loc_name_input = self.folder_selection_state_loc_name_input.format(name)
        element = self.driver.find_element(self.by, loc_name)
        element_input = self.driver.find_element(self.by, loc_name_input)
        if enabled:
            if not element_input.is_selected():
                element.click()
        else:
            if element_input.is_selected():
                element.click()

    def mark_folder(self, name):
        self.__change_folder_selection_state(name, enabled=True)

    def unmark_folder(self, name):
        self.__change_folder_selection_state(name, enabled=False)

#---radio_button.py---
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver


class RadioButton:
    def __init__(self, driver: WebDriver, locator: tuple):
        self.driver: WebDriver = driver
        self.locator = locator
        self.by, self.loc = locator
        self.locator_name = self.loc + "[contains(@for, '{}')]"
        self.locator_input = "//ancestor::div/input"

    def type_of(self) -> str:
        return self.__class__.__name__

    def select(self, name):
        element = self.driver.find_element(self.by, self.locator_name.format(name))
        element.click()

    def is_selected(self, name: str = None) -> bool:
        element_check_locator = self.locator_name.format(name) + self.locator_input
        element = self.driver.find_element(self.by, element_check_locator)
        return element.is_selected()

    def check_radio_button_text_success_status(self, name):
        element = self.driver.find_element(By.XPATH,
                                           f"//span[@class ='text-success'][text()[contains(., '{name.title()}')]]")
        return element.is_displayed()

    def enable_radio_button(self, name):
        enable_element_locator = self.locator_name.format(name) + self.locator_input
        element = self.driver.find_element(self.by, enable_element_locator)
        if not element.is_enabled():
            self.driver.execute_script("arguments[0].disabled = false;", element)
        return self

    def collect_buttons(self):
        loc = '//input[@type="radio"]'
        collected_buttons = self.driver.find_elements(self.by, loc)
        collected_buttons_info = {}
        for elem in collected_buttons:
            name: str = elem.get_attribute("id")[:-5]
            is_selected = elem.is_selected()
            is_enabled = elem.is_enabled()
            collected_buttons_info.update(
                {name: {'is activated': is_enabled, 'is selected': is_selected}})

        return collected_buttons_info

#---page_checkbox.py---
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
# from Homework16CheckBoxes.widgets.expandable_elements import ExpandableTreeElement


class PageCheckbox:
    _instance = None
    URL = "https://demoqa.com/checkbox"
    def __new__(cls, driver: WebDriver):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self, driver: WebDriver):
        self.driver = driver
        self.tree_locator = (By.ID, "tree-node")
        self.tree_element = ExpandableTreeElement(driver=self.driver, locator=self.tree_locator)

    def expand_folder(self, folder_name):
        self.tree_element.expand_folder(name=folder_name)

    def collapse_folder(self, folder_name):
        self.tree_element.collapse_folder(name=folder_name)

    def mark_folder(self, folder_name):
        self.tree_element.mark_folder(name=folder_name)

    def unmark_folder(self, folder_name):
        self.tree_element.unmark_folder(name=folder_name)

    def select_radio_button(self, button_name):
        radio_button_locator = (By.XPATH, f'//label[text()="{button_name}"]')
        radio_button = RadioButton(driver=self.driver, locator=radio_button_locator)
        radio_button.select(name=button_name)

    def is_radio_button_selected(self, button_name):
        radio_button_locator = (By.XPATH, f'//label[text()="{button_name}"]')
        radio_button = RadioButton(driver=self.driver, locator=radio_button_locator)
        return radio_button.is_selected(name=button_name)

"""