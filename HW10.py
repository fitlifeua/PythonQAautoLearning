from typing import Union
import string
import random


def generate_random_integer() -> float:
    """
    Generates a random float number between -10^10 and 10^10.

    Returns:
    --------
    float:
        A random float number between -10^10 and 10^10.
    """
    return random.random() * random.randint(-10**10, 10**10)


def generate_random_string(length: Union[int, float] = 10) -> str:
    """
    Generates a random string of lowercase alphabets with given length.

    Parameters:
    -----------
    length: Union[int, float]
        Length of the random string to be generated. Should be a positive integer or float.
        If not provided or provided as 10, the default length of the string is 10.

    Returns:
    --------
    str:
        A random string of lowercase alphabets with the given length.
    """
    if length <= 0 or not isinstance(length, (int, float)):
        raise ValueError("Length should be a positive integer or float")

    if isinstance(length, float):
        length = round(length)

    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))


if __name__ == '__main__':
    generate_random_string(10)
    generate_random_string(20)
