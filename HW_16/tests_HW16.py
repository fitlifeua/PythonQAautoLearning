from widgets_HW16 import Checkbox, RadioButton
from selenium.webdriver.common.by import By
from selenium import webdriver


def test_checkboxes():
    driver = webdriver.Chrome()
    checkbox_page = Checkbox(driver)
    checkbox_page.navigate()

    elements_to_select = ["Commands", "Home", "Desktop", "General", "Documents", "Office"]
    checkbox_page.select_checkboxes(elements_to_select)

    checkboxes_status = checkbox_page.get_checkboxes_status()
    expected_checkboxes_status = {"Commands": True,
                                  "Home": True,
                                  "Desktop": True,
                                  "General": True,
                                  "Documents": False,
                                  "Office": False}

    assert checkboxes_status == expected_checkboxes_status, f"Expected {expected_checkboxes_status}," \
                                                            f"but got {checkboxes_status}"

    driver.quit()


def test_radiobutton():
    driver = webdriver.Chrome()
    driver.get("https://demoqa.com/radio-button")
    driver.maximize_window()
    radio_button = RadioButton(driver)

    # (test_activate_yes_radio) Активувати кнопку Yes та переконатись що вона активована.
    # Переконатись двома різними способами.
    radio_button.activate_radio_button("Yes")
    assert radio_button.is_radio_button_selected("Yes")
    assert driver.find_element(By.ID, "yesRadio").is_selected()

    # (test_get_radio_buttons_info) Визначити які є радіо-баттони на сторінці та сформувати словник із такими даними:
    # Назва кнопки, Чи увімкнена кнопка, Чи активна (обрана) кнопка
    radio_buttons_info = radio_button.get_radio_buttons_info()
    assert radio_buttons_info == {
        "Yes": {"Enabled": True, "Selected": True},
        "Impressive": {"Enabled": True, "Selected": False},
        "No": {"Enabled": True, "Selected": False},
        "No, its not required": {"Enabled": False, "Selected": False}
    }

    # (Бонус)(test_activate_disabled_radio_button) Увімкнути та активувати кнопку No,
    # переконатись в тому, що вона обрана
    driver.execute_script("arguments[0].removeAttribute('disabled')",
                          driver.find_element(By.XPATH, "//label[@for='noRadio']"))
    radio_button.activate_radio_button("No")
    assert radio_button.is_radio_button_selected("No")
    assert driver.find_element(By.ID, "noRadio").is_selected()
    driver.quit()
