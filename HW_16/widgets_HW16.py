from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Checkbox:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)
        self.url = "https://demoqa.com/checkbox"

    def navigate(self):
        self.driver.get(self.url)
        self.driver.maximize_window()

    def select_checkboxes(self, element_names):
        for element_name in element_names:
            checkbox = self.wait.until(EC.presence_of_element_located((By.XPATH, f"//span[text()='{element_name}']/preceding-sibling::span")))
            checkbox.click()

    def get_checkboxes_status(self):
        checkboxes = self.wait.until(EC.presence_of_all_elements_located((By.XPATH, "//span[@class='rct-title']")))
        checkbox_status = {}
        for checkbox in checkboxes:
            checkbox_name = checkbox.text
            checkbox_checked = checkbox.find_element(By.XPATH, "./preceding-sibling::span").get_attribute("class") == "rct-icon rct-icon-check"
            checkbox_status[checkbox_name] = checkbox_checked
        return checkbox_status


class RadioButton:
    def __init__(self, driver):
        self.driver = driver
        self.radio_buttons = {
            "Yes": (By.XPATH, "//label[@for='yesRadio']"),
            "Impressive": (By.XPATH, "//label[@for='impressiveRadio']"),
            "No": (By.XPATH, "//label[@for='noRadio']"),
            "No, its not required": (By.XPATH, "//label[@for='noRadio']//following-sibling::label")
        }

    def activate_radio_button(self, button_name):
        button = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.radio_buttons[button_name]))
        button.click()

    def is_radio_button_selected(self, button_name):
        button = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.radio_buttons[button_name]))
        return button.find_element(By.XPATH, "./preceding-sibling::input").is_selected()

    def get_radio_buttons_info(self):
        radio_buttons_data = {}
        for button_name in self.radio_buttons:
            button = WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located(self.radio_buttons[button_name]))
            input_element = button.find_element(By.XPATH, "./preceding-sibling::input")
            enabled = input_element.is_enabled()
            selected = input_element.is_selected()
            radio_buttons_data[button_name] = {"Enabled": enabled, "Selected": selected}
        return radio_buttons_data
