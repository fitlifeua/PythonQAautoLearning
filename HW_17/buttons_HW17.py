from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


class DynamicPropertiesPage:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(driver, 5)

    def get_enable_button(self):
        locator = (By.ID, "enableAfter")
        return self.wait.until(EC.element_to_be_clickable(locator))

    def get_title_color_button(self):
        locator = (By.ID, "colorChange")
        self.wait.until(EC.text_to_be_present_in_element_attribute(locator, "class", "text-danger"))
        return self.driver.find_element(*locator)

    def get_visible_button(self):
        locator = (By.ID, "visibleAfter")
        return self.wait.until(EC.visibility_of_element_located(locator))
