from HW_17.buttons_HW17 import DynamicPropertiesPage


# Test 1: Переконується що кнопка "Will enable 5 seconds" вмикається
def test_button_becomes_clickable(driver_hw17):
    dynamic_page = DynamicPropertiesPage(driver_hw17)
    enable_button = dynamic_page.get_enable_button()
    assert enable_button.is_enabled()


# Test 2: Переконується що колір напису на кнопці "Color change" змінюється
def test_title_color_changes(driver_hw17):
    dynamic_page = DynamicPropertiesPage(driver_hw17)
    title_color_button = dynamic_page.get_title_color_button()
    assert "text-danger" in title_color_button.get_attribute("class")


# Test 3: Переконується що кнопка "Visible After 5 Seconds" з'являється
def test_button_becomes_visible(driver_hw17):
    dynamic_page = DynamicPropertiesPage(driver_hw17)
    visible_button = dynamic_page.get_visible_button()
    assert visible_button.is_displayed()
