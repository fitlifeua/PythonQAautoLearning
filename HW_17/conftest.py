import pytest
from selenium import webdriver


@pytest.fixture(scope="module")
def driver_hw17():
    options = webdriver.ChromeOptions()
    options.add_argument('--headless')
    driver = webdriver.Chrome(options=options)
    driver.get("https://demoqa.com/dynamic-properties")
    yield driver
    driver.quit()
