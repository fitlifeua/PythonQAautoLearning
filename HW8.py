# 1. Напишіть декоратор, який визначає час виконання функції.
# Заміряйте час іиконання функцій з попереднього ДЗ

from lib import date_validation, season_checker, stupid_calculator, time_checker

"""
Я тут трохи переробив структуру проєкту. А саме, я декоратор також виніс у lib.py файл.
Тому для виконання основної функції не треба додатково імпортувати бібліотеку time, та писати декоратор у цьому файлі.
Крім того, це дозволило уникнути "забруднення" коду тому що, для запуску декоратора для імпортованої функції необхідно
виконувати імпорт бібліотеки time -> потім писати декоратор -> потім робити імпорт самої функції.
"""


@time_checker
def hw8_task1(num1, num2, operator):
    return stupid_calculator(num1, num2, operator)


"""
Для відпрацювання функції необхідно у змінні num1, num2, operator записати значення для виконання функції.
"""

num12 = 5
num13 = 2.5
operator = "*"

result = hw8_task1(num12, num13, operator)

if result is not None:
    print("Відповідь: ", result)


# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання

date = input("Будь-ласка введіть дату у форматі '[дд].[мм]': ")
if date_validation(date):
    print(season_checker(date))
else:
    print("Такої дати не існує")
