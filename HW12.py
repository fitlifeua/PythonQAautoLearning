"""створити ієрархію класів (реалізувати наслідування) гаджетів (наприклад, телефон та ноутбук
зі спільним абстрактним предком)
створити фікстури відповідних сутностей та покрити їх тестами"""


from abc import ABC, abstractmethod

class Gadget(ABC):
    def __init__(self, brand, model, year, os, price):
        self.brand = brand
        self.model = model
        self.year = year
        self.os = os
        self.price = price

    @abstractmethod
    def get_info(self):
        pass

class Phone(Gadget):
    def __init__(self, brand, model, year, os, price, memory, camera):
        super().__init__(brand, model, year, os, price)
        self.memory = memory
        self.camera = camera

    def get_info(self):
        return f"The phone is: {self.brand} {self.model} {self.os} ({self.year}) - ${self.price}, {self.memory}, {self.camera}"

class Laptop(Gadget):
    def __init__(self, brand, model, year, os, price, ram, screen_size):
        super().__init__(brand, model, year, os, price)
        self.ram = ram
        self.screen_size = screen_size

    def get_info(self):
        return f"The laptop is: {self.brand} {self.model} {self.os} ({self.year}) - ${self.price}, {self.ram}, {self.screen_size}"

class SmartWatch(Gadget):
    def __init__(self, brand, model, year, price, os, battery, dial_size):
        super().__init__(brand, model, year, os, price)
        self.battery = battery
        self.dial_size = dial_size

    def get_info(self):
        return f"The smart watch is: {self.brand} {self.model} {self.os} ({self.year}) - ${self.price}, {self.battery}, {self.dial_size}"
