"""Підключіться до API НБУ (документація тут https://bank.gov.ua/ua/open-data/api-dev),
отримайте курс валют і запишіть його в текстовий файл такому форматі (список):

"[дата створення запиту]"

1. [назва валюти 1] to UAH: [значення курсу до валюти 1]

2. [назва валюти 2] to UAH: [значення курсу до валюти 2]

3. [назва валюти 3] to UAH: [значення курсу до валюти 3]

...

n. [назва валюти n] to UAH: [значення курсу до валюти n]

P.S.не забувайте про DRY, KISS, SRP та перевірки"""


import requests

from datetime import datetime


def get_exchange_rates():
    url = "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    response = requests.get(url)
    data = response.json()
    exchange_rates = []
    for currency in data:
        name = currency['txt']
        rate = currency['rate']
        exchange_rates.append(f"{name} to UAH: {rate}")
    return exchange_rates


def write_exchange_rates_to_file(exchange_rates, filename="exchange_rates.txt"):
    with open(filename, "w") as file:
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        file.write(f"[{now}]\n")
        for i, rate in enumerate(exchange_rates):
            file.write(f"{i + 1}. {rate}\n")
