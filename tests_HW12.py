class TestPhone:
    def test_phone_get_info(self, phone1, phone2):
        assert phone1.get_info() == 'The phone is: Apple iPhone 14 Pro iOS 15 (2022) - $999, 128GB, 12MP'
        assert phone2.get_info() == 'The phone is: Samsung Galaxy S22 Ultra Android 12 (2022) - $1199, 128GB, 108MP'


class TestLaptop:
    def test_laptop_get_info(self, laptop1, laptop2):
        assert laptop1.get_info() == 'The laptop is: Apple MacBook Pro macOS Monterey (2022) - $1299, 8GB, 13-inch'
        assert laptop2.get_info() == 'The laptop is: ASUS ProArt Studiobook 16 Windows (2021) - $1999, 64GB, 16-inch'


class TestSmartWatch:
    def test_smartwatch_get_info(self, smartwatch1, smartwatch2):
        assert smartwatch1.get_info() == 'The smart watch is: Apple Watch Series 8 (2022) - $399, watchOS 09, 18 hours, 45mm'
        assert smartwatch2.get_info() == 'The smart watch is: Samsung Galaxy Watch 5 Pro (2022) - $449, Tizen OS 6.0, 40 hours, 45mm'

