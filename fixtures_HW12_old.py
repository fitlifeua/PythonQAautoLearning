from HW12_old import Gadget, Phone, Laptop, SmartWatch

phone1 = Phone('Apple', 'iPhone 14 Pro', 2022, 'iOS 15', 999, '128GB', '12MP')
phone2 = Phone('Samsung', 'Galaxy S22 Ultra', 2022, 'Android 12', 1199, '128GB', '108MP')

laptop1 = Laptop('Apple', 'MacBook Pro', 2022, 'macOS Monterey', 1299, '8GB', '13-inch')
laptop2 = Laptop('ASUS', 'ProArt Studiobook 16', 2021, 'Windows', 1999, '64GB', '16-inch')

smartwatch1 = SmartWatch('Apple', 'Watch Series 8', 2022, 399, 'watchOS 09', '18 hours', '45mm')
smartwatch2 = SmartWatch('Samsung', 'Galaxy Watch 5 Pro', 2022, 449, 'Tizen OS 6.0', '40 hours', '45mm')
