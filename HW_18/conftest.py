import pytest

API_HOSTS = {
    'test': 'https://gitlab.com/api/v4/projects/'
}

@pytest.fixture(scope="module")
def base_url():
    return API_HOSTS['test']
