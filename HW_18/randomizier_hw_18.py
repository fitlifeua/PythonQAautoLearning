import random
import string


def generate_random_string(length):
    """
    Generates a random string of the specified length.

    Args:
        length (int): The length of the generated string.

    Returns:
        str: The randomly generated string.

    """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length))
