import pytest
import requests

from HW_18.randomizier_hw_18 import generate_random_string


"""
a. Отримання інформації з API: 
Напишіть функцію або метод, який виконує GET-запит до API і повертає відповідь сервера.
Ви можете використати метод requests.get() для цього. Виведіть отримані дані на екран або збережіть їх у файл
"""


def test_get_project_data_from_api(base_url):
    project_id = "42967281"
    url_case1 = f"{base_url}{project_id}"
    response = requests.get(url_case1)
    project_data = response.json()
    print(project_data)


"""
b. Надсилання даних на API: Напишіть функцію або метод, який виконує POST-запит до API з деякими вхідними даними
 і повертає відповідь сервера. Ви можете використати метод requests.post() для цього.
 Виведіть отримані дані на екран або збережіть їх у файл.
"""


def test_post_create_new_project(base_url):
    url_case2 = base_url
    headers = {
        "Private-Token": "glpat-ykHxKFGa1eNDFdPreH2U"
    }
    unique_random = generate_random_string(2)
    payload = {
        "name": f"api-project-{unique_random}",
        "description": "This is a new project created via API"
    }
    response = requests.post(url_case2, json=payload, headers=headers)
    new_project_data = response.json()
    print(new_project_data)


"""
c. Параметризоване тестування: Напишіть функцію або метод, який приймає параметри
(наприклад, URL, метод, тіло запиту) і виконує відповідний запит до API.
Використовуйте параметри, які надаються як аргументи функції або методу. Поверніть результат виконання запиту.
"""

url_case3 = "https://gitlab.com/api/v4/projects/46382134/feature_flags"
method_post = "POST"
method_get = "GET"
method_delete = "DELETE"
request_body = {"name": f"awesome_feature_{generate_random_string(3)}", "version": "new_version_flag"}


@pytest.mark.parametrize("url_case3, method, request_body", [
    ("https://gitlab.com/api/v4/projects/46382134/feature_flags", method_post,
     {"name": f"awesome_feature_{generate_random_string(3)}", "version": "new_version_flag"}),
    ("https://gitlab.com/api/v4/projects/46382134/feature_flags", method_get,
     {"name": f"another_feature_{generate_random_string(3)}", "version": "new_version_flag"}),
    ("https://gitlab.com/api/v4/projects/46382134/feature_flags", method_delete,
     {"name": f"third_feature_{generate_random_string(3)}", "version": "new_version_flag"})
])
def test_api_request(url_case3, method, request_body):
    headers = {
        "Private-Token": "glpat-ykHxKFGa1eNDFdPreH2U",
        "Content-type": "application/json"
    }

    response_case3 = requests.request(method, url_case3, json=request_body, headers=headers)
    result = response_case3.json()
    assert response_case3.status_code == 201

    return result
