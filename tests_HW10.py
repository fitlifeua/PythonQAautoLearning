import pytest
from HW10 import generate_random_integer, generate_random_string


def test_generate_random_integer_returns_float():
    num = generate_random_integer()
    assert isinstance(num, float)


def test_generate_random_integer_within_range():
    num = generate_random_integer()
    assert -10**10 <= num <= 10**10


def test_generate_random_string_returns_string():
    length = 10
    s = generate_random_string(length)
    assert isinstance(s, str)


def test_generate_random_string_lowercase():
    length = 10
    s = generate_random_string(length)
    assert s.islower()


def test_generate_random_string_length():
    length = 10
    s = generate_random_string(length)
    assert len(s) == length


def test_generate_random_string_float_length():
    length = 7.0
    s = generate_random_string(length)
    assert len(s) == int(length)


def test_generate_random_string_zero_length():
    with pytest.raises(ValueError):
        generate_random_string(0)


def test_generate_random_string_negative_length():
    with pytest.raises(ValueError):
        generate_random_string(-5)


def test_generate_random_string_non_numeric_length():
    with pytest.raises(ValueError):
        generate_random_string("letters")
