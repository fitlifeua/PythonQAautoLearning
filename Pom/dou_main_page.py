from selenium.webdriver.common.by import By

class DouMainPage:
    def __init__(self, driver):
        self.dou_logo = driver.find_element(By.XPATH, "//a[contains(@href, 'https://dou.ua/')]")
        self.golovna_page_link = driver.find_element(By.XPATH, "//a[contains(text(),'Головна')]")
        self.login_signin_link = driver.find_element(By.XPATH, "//a[@id='login-link']")
        self.login_use_social_network_popup = driver.find_element(By.XPATH, "(.//*[normalize-space(text()) and normalize-space(.)='Cкористайтесь акаунтом'])[1]/following::div[2]")
