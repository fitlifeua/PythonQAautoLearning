from selenium.webdriver.common.by import By


class LoginPopup:
    def __init__(self, driver):
        self.page = driver
        self.login_by_mail_link = driver.find_element(By.XPATH, "xpath=//a[@id='_loginByMail']")
        self.username_input = driver.find_element(By.XPATH, "xpath=//input[@name='username']")
        self.password_input = driver.find_element(By.XPATH, "xpath=//input[@name='password']")
        self.enter_button = driver.find_element(By.XPATH, "xpath=button.big-button.btnSubmit")

    def test_user_login(self, username="TestUser", password="test123"):
        self.login_by_mail_link.click()
        self.username_input.send_keys(username)
        self.password_input.send_keys(password)
        self.enter_button.click()