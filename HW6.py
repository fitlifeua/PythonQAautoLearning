# 1. Напишіть функцію, яка приймає два аргументи.
#    a) Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
#    b) Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
#    c) В будь-якому іншому випадку - функція повертає кортеж з двох агрументів

print('Завдання 1')


def task1_function(add_arg1, add_arg2):
    if type(add_arg1) in [int, float] and type(add_arg2) in [int, float]:
        result = add_arg1 * add_arg2
        print(f"Обидва аргументи є числовими типами. Результат: {result}")
    elif type(add_arg1) is str and type(add_arg2) is str:
        result = add_arg1 + add_arg2
        print(f"Обидва аргументи є стрінгами. Результат: {result}")
    else:
        result = (add_arg1, add_arg2)
        print(f"Аргументи не є числами чи стрінгами. Результат: {result}")
    return result


# Перевірка виконання задачі 1
task1_function(2, 2)
task1_function(2, 2.5)
task1_function("Ukraine", " is winning")
task1_function("Ukraine is number", 1)

# 2. Візьміть попереднє дз "Касир в кінотеатрі" і перепишіть за допомогою функцій. Памʼятайте про SRP!

print('\nЗавдання 2')


def age_validate(client_age_validation):
    if not client_age_validation.isdigit() or int(client_age_validation) <= 0:
        return False
    return True


def age_control(client_age):
    if str(7) in str(client_age):
        print("Вам сьогодні пощастить!")
    elif client_age < 7:
        print('Де твої батьки?')
    elif client_age < 16:
        print('Цей фільм для дорослих!')
    elif 65 <= client_age <= 100:
        print('Покажіть пенсійне посвідчення!')
    else:
        print('А білетів вже немає!')


# Перевірка виконання задачі 2
checking_age = input('Введіть вік клієнта: ')
if age_validate(checking_age):
    age = int(checking_age)
    age_control(age)
else:
    print('Увага! Вік має бути тільки позитивним числом')
