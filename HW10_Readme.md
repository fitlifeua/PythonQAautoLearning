# **HW10**

# **Random String and Integer Generator**

This is a Python module that generates random strings and integers.

## **Installation**

To use this module, make sure you have Python installed on your system. Clone the repository to your local machine using the following command:

`git clone https://gitlab.com/fitlifeua/PythonQAautoLearning/-/blob/master/HW10.py`

After cloning the repository, navigate to the project directory and install the required packages using the following command:

`pip install -r requirements.txt`

## **Usage**

The generate_random_string() function generates a random string of lowercase alphabets with a given length (default length is 10).

`from HW10 import generate_random_string`

'# Generate a random string of length 10'

`random_str = generate_random_string()`

'# Generate a random string of length 20'

`random_str = generate_random_string(20)`

**The generate_random_integer() function generates a random float number between -10^10 and 10^10.**

`from HW10 import generate_random_integer`

'# Generate a random float number between -10^10 and 10^10'

`random_int = generate_random_integer()`

## Testing

To run the tests, navigate to the project directory and run the following command:

`pytest`

The tests will run, and the output will be displayed on the console.

## License

This project is licensed under free license :)
