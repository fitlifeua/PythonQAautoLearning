def test_hw14(wo_login_user):
    """
        Test the main page of the DOU website for the presence and visibility of certain elements.

        The test navigates to the DOU website using the wo_login_user fixture, which opens the website
        and returns a DouMainPage object.
        The test then asserts the presence and visibility of the:
        - DOU logo,
        - the "Головна" link,
        - the "Увійти" link.
        It also asserts that the "Головна" link has the "sel" class attribute.
        Finally, the test clicks on the "Увійти" link to open the login popup and asserts the presence and
        visibility of the "Увійти через соцмережі" button.

        Args:
            wo_login_user: A fixture that opens the DOU website and returns a DouMainPage object.

        Returns:
            None
        """

    dou_main_page = wo_login_user

    assert dou_main_page.dou_logo.is_displayed()
    assert dou_main_page.golovna_page_link.is_displayed()
    selected_page_class_attribute = dou_main_page.golovna_page_link.get_attribute("class")
    assert selected_page_class_attribute == "sel"
    assert dou_main_page.login_signin_link.is_displayed()
    dou_main_page.login_signin_link.click()
    assert dou_main_page.login_use_social_network_popup.is_displayed()
